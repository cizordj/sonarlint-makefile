Download the latest version of sonarlint
here:
https://github.com/SonarSource/sonarlint-vscode/releases/

Unpack the zip file here and then run:
sudo make install

Don't forget to install a compatible java RE.

sudo apt install default-jre
