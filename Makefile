PREFIX ?= /usr/local
DESTDIR ?=/usr/local/share/sonarlint
BINDIR ?= $(PREFIX)/bin
LATEST_VERSION_URL = https://github.com/SonarSource/sonarlint-vscode/releases/download/4.17.0%2B77356/sonarlint-vscode-linux-x64-4.17.0.vsix
TMP_FILE = sonarlint.zip

all:
	wget "$(LATEST_VERSION_URL)" -O "$(TMP_FILE)"
	unzip "$(TMP_FILE)"
	rm "$(TMP_FILE)"
clean:
	@rm -rf extension
	@rm '[Content_Types].xml'
	@rm extension.vsixmanifest

install:
	@install -v -d "$(DESTDIR)"
	@install -v -d "$(DESTDIR)/analyzers"
	@install -v -d "$(DESTDIR)/server"
	@install -m 0644 -v "extension/server/sonarlint-ls.jar" "$(DESTDIR)/server/sonarlint-ls.jar"
	@install -m 0755 -v "bin/sonarlint" "$(BINDIR)/sonarlint"
	@install -m 0755 -v "bin/sonarlint-lsp" "$(BINDIR)/sonarlint-lsp"
	@install -m 0644 -v "extension/analyzers/sonarcfamily.jar" "$(DESTDIR)/analyzers/"
	@install -m 0644 -v "extension/analyzers/sonargo.jar" "$(DESTDIR)/analyzers/"
	@install -m 0644 -v "extension/analyzers/sonarhtml.jar" "$(DESTDIR)/analyzers/"
	@install -m 0644 -v "extension/analyzers/sonariac.jar" "$(DESTDIR)/analyzers/"
	@install -m 0644 -v "extension/analyzers/sonarjava.jar" "$(DESTDIR)/analyzers/"
	@install -m 0644 -v "extension/analyzers/sonarjavasymbolicexecution.jar" "$(DESTDIR)/analyzers/"
	@install -m 0644 -v "extension/analyzers/sonarjs.jar" "$(DESTDIR)/analyzers/"
	@install -m 0644 -v "extension/analyzers/sonarlintomnisharp.jar" "$(DESTDIR)/analyzers/"
	@install -m 0644 -v "extension/analyzers/sonarphp.jar" "$(DESTDIR)/analyzers/"
	@install -m 0644 -v "extension/analyzers/sonarpython.jar" "$(DESTDIR)/analyzers/"
	@install -m 0644 -v "extension/analyzers/sonartext.jar" "$(DESTDIR)/analyzers/"
	@install -m 0644 -v "extension/analyzers/sonarxml.jar" "$(DESTDIR)/analyzers/"

uninstall:
	@rm \
		"$(DESTDIR)/server/sonarlint-ls.jar" \
		"$(DESTDIR)/analyzers/sonarcfamily.jar" \
		"$(DESTDIR)/analyzers/sonargo.jar" \
		"$(DESTDIR)/analyzers/sonarhtml.jar" \
		"$(DESTDIR)/analyzers/sonariac.jar" \
		"$(DESTDIR)/analyzers/sonarjava.jar" \
		"$(DESTDIR)/analyzers/sonarjavasymbolicexecution.jar" \
		"$(DESTDIR)/analyzers/sonarjs.jar" \
		"$(DESTDIR)/analyzers/sonarlintomnisharp.jar" \
		"$(DESTDIR)/analyzers/sonarphp.jar" \
		"$(DESTDIR)/analyzers/sonarpython.jar" \
		"$(DESTDIR)/analyzers/sonartext.jar" \
		"$(DESTDIR)/analyzers/sonarxml.jar" \
		"$(BINDIR)/sonarlint-lsp" \
		"$(BINDIR)/sonarlint"
	-@rmdir \
		"$(DESTDIR)/analyzers" \
		"$(DESTDIR)/server" \
		"$(DESTDIR)"
